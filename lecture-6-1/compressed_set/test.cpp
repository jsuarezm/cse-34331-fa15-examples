#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>

#include "compressed_set.hpp"

using namespace std;

int main() {
  ifstream dictfile("/usr/share/dict/words");
  string line;
  const double epsilon = 1e-3;
  compressed_set<string> dict(235886, epsilon);
  const int trials = 1000000;
  int errors = 0;

  while (getline(dictfile, line)) {
    dict.insert(line);
  }

  cout << "dictionary uses " << dict.num_bits()/8 << " bytes\n";
  cout << "theoretical error rate is " << epsilon << "\n";

  for (int i=0; i<trials; i++) {
    ostringstream ss;
    ss << "foobar" << rand();
    if (dict.count(ss.str()) > 0) errors++;
  }

  cout << "actual error rate is " << static_cast<float>(errors)/trials << "\n";
}
