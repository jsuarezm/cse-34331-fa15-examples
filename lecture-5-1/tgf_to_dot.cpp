#include <iostream>
#include <map>
#include <set>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

template <typename NodeLabel, typename EdgeLabel>
struct Graph {
    vector <NodeLabel> labels;
    vector <map<int, EdgeLabel>> edges;
};

template <typename NodeLabel, typename EdgeLabel>
void load_graph(Graph<NodeLabel, EdgeLabel> &g) {
    string line;

    // Clear Graph
    g.labels.clear();
    g.edges.clear();

    // Read labels (vertices)
    g.labels.push_back("");

    while (getline(cin, line) && line[0] != '#') {
	stringstream ss(line);
	int node;
	string label;

	ss >> node;
	getline(ss, label);
	g.labels.push_back(label);
    }

    // Read edges (vertices)
    g.edges.resize(g.labels.size());

    while (getline(cin, line)) {
	stringstream ss(line);
	int source;
	int target;
	string label;

	ss >> source >> target;
	getline(ss, label);

	g.edges[source][target] = label;
    }
}

template <typename NodeLabel, typename EdgeLabel>
void dump_graph(Graph<NodeLabel, EdgeLabel> &g) {
    cout << "digraph tgf {" << endl;

    for (size_t v = 1; v < g.labels.size(); v++) {
	for (auto &edge : g.edges[v]) {
	    cout << "\t" << v << " -> " << edge.first << ";" << endl;
	}
    }

    cout << "}" << endl;
}

int main(int argc, char *argv[]) {
    Graph<string, string> g;

    load_graph(g);
    dump_graph(g);

    return 0;
};
