// stackoverflow.cpp: too much recursion

int factorial(int x)
{
    if (x == 1) {
    	return 1;
    }

    return x * factorial(x - 1);
}

int main(int argc, char *argv[])
{
    factorial(1 << 20);
    return 0;
}
