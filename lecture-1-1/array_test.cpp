#include <cassert>
#include <cstdlib>
#include <iostream>

#include "array.h"

template <typename T>
bool is_equal(Array<T> &a, Array<T> &b)
{
    if (a.size() != b.size()) {
    	return false;
    }

    for (int i = 0; i < a.size(); i++) {
    	if (a[i] != b[i])
    	    return false;
    }

    return true;
}

int main(int argc, char *argv[])
{
    Array<int> a0(100);
    Array<int> a1(a0);
    Array<int> a2 = a0;

    std::cout << "a0 == a1 ? " << std::boolalpha << is_equal(a0, a1) << std::endl;
    std::cout << "a0 == a2 ? " << std::boolalpha << is_equal(a0, a2) << std::endl;
    std::cout << "a1 == a2 ? " << std::boolalpha << is_equal(a1, a2) << std::endl;

    a0[0] = rand(); std::cout << "* a0[0] = rand()" << std::endl;
    
    std::cout << "a0 == a1 ? " << std::boolalpha << is_equal(a0, a1) << std::endl;
    std::cout << "a0 == a2 ? " << std::boolalpha << is_equal(a0, a2) << std::endl;
    std::cout << "a1 == a2 ? " << std::boolalpha << is_equal(a1, a2) << std::endl;

    a1 = a0; std::cout << "* a1 = a0" << std::endl;
    
    std::cout << "a0 == a1 ? " << std::boolalpha << is_equal(a0, a1) << std::endl;
    std::cout << "a0 == a2 ? " << std::boolalpha << is_equal(a0, a2) << std::endl;
    std::cout << "a1 == a2 ? " << std::boolalpha << is_equal(a1, a2) << std::endl;

    return 0;
}
