// memoryleak.cpp: where is my mind

#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <iostream>
#include <vector>

#include "array.h"

const int NITEMS = 1<<10;
const int TRIALS = 100;

bool duplicates(int n)
{
    Array<int> randoms(n);

    for (int i = 0; i < NITEMS; i++) {
    	randoms[i] = rand();
    }

    for (int i = 0; i < n; i++) {
    	int *begin = &randoms[i + 1];
    	int *end   = &randoms[0] + NITEMS;

	if (std::find(begin, end, randoms[i]) != end) {
	    return true;
	}
    }

    return false;
}

int main(int argc, char *argv[])
{
    srand(time(NULL));

    for (int i = 0; i < TRIALS; i++) {
    	if (duplicates(NITEMS)) {
	    std::cout << "Duplicates Detected!" << std::endl;
	}
    }

    return 0;
}
