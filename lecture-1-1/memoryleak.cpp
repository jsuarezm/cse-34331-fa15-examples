// memoryleak.cpp: where is my mind

#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <iostream>
#include <vector>

const int NITEMS = 1<<10;
const int TRIALS = 100;

bool duplicates(int n)
{
    int *randoms = new int[n];

    for (int i = 0; i < NITEMS; i++) {
    	randoms[i] = rand();
    }

    for (int i = 0; i < n; i++) {
	if (std::find(randoms + i + 1, randoms + NITEMS, randoms[i]) != (randoms + NITEMS)) {
	    return true;
	}
    }

    delete []randoms;
    return false;
}

int main(int argc, char *argv[])
{
    srand(time(NULL));

    for (int i = 0; i < TRIALS; i++) {
    	if (duplicates(NITEMS)) {
	    std::cout << "Duplicates Detected!" << std::endl;
	}
    }

    return 0;
}
