// duplicates_linear.cpp: simple duplicates check

#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <iostream>
#include <vector>

const int NITEMS = 1<<15;

int main(int argc, char *argv[])
{
    std::vector<int> v;

    srand(time(NULL));

    for (int i = 0; i < NITEMS; i++) {
    	v.push_back(rand());
    }

    for (auto it = v.begin(); it != v.end(); it++) {
	if (std::find(it + 1, v.end(), *it) != v.end()) {
	    std::cout << *it << " is duplicated!" << std::endl;
	    break;
	}
    }

    return 0;
}
