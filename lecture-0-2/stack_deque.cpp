// stack_deque.cpp: deque stack

#include <deque>
#include <stack>

const int NITEMS = 1<<25;

int main(int argc, char *argv[])
{
    std::stack<int, std::deque<int>> s;

    for (int i = 0; i < NITEMS; i++) {
    	s.push(i);
    }

    while (!s.empty()) {
    	s.pop();
    }

    return 0;
}
