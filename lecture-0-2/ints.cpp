// ints.cpp: int ADT example

#include <climits>
#include <iostream>

int main(int argc, char *argv[]) {
    std::cout << "Size of int: " << sizeof(int) << " bytes" << std::endl
	      << "Maximum int: " << INT_MAX << std::endl
	      << "Minimum int: " << INT_MIN << std::endl;

    std::cout << "Overflow:  " << INT_MAX + 1 << std::endl;
    std::cout << "Underflow: " << INT_MIN - 1 << std::endl;

    return 0;
}
