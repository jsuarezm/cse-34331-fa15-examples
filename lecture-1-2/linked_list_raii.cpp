// linked_list_raii.cpp: Singly Linked List (RAII)

#include <cstdlib>
#include <iostream>
#include <stdexcept>

const int NITEMS = 10;

// List declaration ------------------------------------------------------------

template <typename T>
class List {
    protected:
        typedef struct Node {
            struct Node *next;
            T            data;
        } Node;

        typedef Node * iterator;

        Node  *head;
        size_t length;

    public:
        List() : head(NULL), length(0) {}
        iterator front() { return head; };

        ~List();                                    // Destructor
        List(const List<T> &source);                // Copy Constructor
        List<T>& operator=(const List<T> &source);  // Assignment Operator

        void clear();                               // Utility: remove all nodes
        void copy(const List<T> &source);           // Utility: copy all nodes

        size_t size() const { return length; }
        T& at(const size_t i);
        void insert(iterator it, const T &data);
        void push_back(const T &data);
        void erase(iterator it);
};

// List implementation --------------------------------------------------------

// Post-condition: Clears all nodes from list
template <typename T>
List<T>::~List() {
    clear();
}

// Post-condition: Copies all nodes from source
template <typename T>
List<T>::List(const List<T> &source) {
    copy(source);
}

// Post-condition: Clears existing list and copies all nodes from source
template <typename T>
List<T>& List<T>::operator=(const List<T> &source) {
    // Need to handle self-assignment
    if (this != &source) {
        clear();
        copy(source);
    }

    return *this;
}

// Post-condition: Deallocates all nodes in list
template <typename T>
void List<T>::clear() {
    Node *next = NULL;

    // Need next otherwise invalid reads (use valgrind)
    for (Node *curr = head; curr != NULL; curr = next) {
        next = curr->next;
        delete curr;
    }
}

// Post-condition: Appends all nodes from source
template <typename T>
void List<T>::copy(const List<T> &source) {
    // Need to initialize because we are starting from scratch (could use
    // initializer syntax)
    head   = NULL;
    length = 0;

    for (Node *curr = source.head; curr != NULL; curr = curr->next) {
        push_back(curr->data);
    }
}

template <typename T>
T& List<T>::at(const size_t i) {
    Node *node = head;
    size_t   n = 0;

    while (n < i && node != NULL) {
        node = node->next;
        n++;
    }

    if (node) {
        return node->data;
    } else {
        throw std::out_of_range("invalid list index");
    }
}

template <typename T>
void List<T>::insert(iterator it, const T& data) {
    if (head == NULL && it == NULL) {
        head = new Node{NULL, data};
    } else {
        it->next = new Node{it->next, data};
    }
    length++;
}

template <typename T>
void List<T>::push_back(const T& data) {
    if (head == NULL) {
        head = new Node{NULL, data};
    } else {
        Node *curr = head;
        Node *tail = head;

        while (curr) {
            tail = curr;
            curr = curr->next;
        }

        tail->next = new Node{NULL, data};
    }
    length++;
}

template <typename T>
void List<T>::erase(iterator it) {
    if (it == NULL) {
	throw std::out_of_range("invalid iterator");
    }

    if (head == it) {
	head = head->next;
	delete it;
    } else {
	Node *node = head;

	while (node != NULL && node->next != it) {
	    node = node->next;
	}

	if (node == NULL) {
	    throw std::out_of_range("invalid iterator");
	}

	node->next = it->next;
	delete it;
    }

    length--;
}

// Main execution -------------------------------------------------------------

int main(int argc, char *argv[]) {
    List<int> list;

    std::cout << "List Size: " << list.size() << std::endl;

    for (int i = 0; i < NITEMS; i++) {
        list.push_back(i);
    }

    std::cout << "List Size: " << list.size() << std::endl;
    std::cout << "List Items:" << std::endl;
    for (size_t i = 0; i < list.size(); i++) {
        std::cout << "List at " << i << " " << list.at(i) << std::endl;
    }

    auto head = list.front();
    list.insert(head, NITEMS + 1);
    list.insert(head, NITEMS + 2);
    list.insert(head->next->next, NITEMS + 3);

    std::cout << "List Size: " << list.size() << std::endl;
    std::cout << "List Items:" << std::endl;
    for (size_t i = 0; i < list.size(); i++) {
        std::cout << "List at " << i << " " << list.at(i) << std::endl;
    }

    // List copy
    List<int> copy(list);

    std::cout << "Copy Size: " << copy.size() << std::endl;
    std::cout << "Copy Items:" << std::endl;
    for (size_t i = 0; i < copy.size(); i++) {
        std::cout << "Copy at " << i << " " << copy.at(i) << std::endl;
    }

    // List assignment
    copy.push_back(NITEMS + 4);
    list = copy;
    std::cout << "List Size: " << list.size() << std::endl;
    std::cout << "List Items:" << std::endl;
    for (size_t i = 0; i < list.size(); i++) {
        std::cout << "List at " << i << " " << list.at(i) << std::endl;
    }


    list.erase(list.front());
    list.erase(list.front()->next);
    std::cout << "List Size: " << list.size() << std::endl;
    std::cout << "List Items:" << std::endl;
    for (size_t i = 0; i < list.size(); i++) {
        std::cout << "List at " << i << " " << list.at(i) << std::endl;
    }

    return 0;
}
