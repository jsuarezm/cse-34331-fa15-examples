// linked_list_tail.cpp: Singly Linked List (tail)

#include <cstdlib>
#include <iostream>
#include <stdexcept>

const int NITEMS = 10;

// List declaration ------------------------------------------------------------

template <typename T>
class List {
    protected:
        typedef struct Node {
            struct Node *next;
            T            data;
        } Node;

        typedef Node * iterator;

        Node   head;
        Node  *tail;    // Tail pointer
        size_t length;

    public:
        List() : head({NULL}), tail(&head), length(0) {}    // Initiailize tail
        iterator front() { return head.next; };
        iterator back()  { return tail; };                  // New property

        ~List();
        List(const List<T> &source);
        List<T>& operator=(const List<T> &source);

        void clear();
        void copy(const List<T> &source);

        size_t size() const { return length; }
        T& at(const size_t i);
        void insert(iterator it, const T &data);
        void push_back(const T &data);
        void erase(iterator it);
};

// List implementation --------------------------------------------------------

template <typename T>
List<T>::~List() {
    clear();
}

template <typename T>
List<T>::List(const List<T> &source) {
    copy(source);
}

template <typename T>
List<T>& List<T>::operator=(const List<T> &source) {
    if (this != &source) {
        clear();
        copy(source);
    }

    return *this;
}

template <typename T>
void List<T>::clear() {
    Node *next = NULL;
    for (Node *curr = head.next; curr != NULL; curr = next) {
        next = curr->next;
        delete curr;
    }
}

template <typename T>
void List<T>::copy(const List<T> &source) {
    head   = {NULL};
    tail   = &head;     // Set to head
    length = 0;

    for (Node *curr = source.head.next; curr != NULL; curr = curr->next) {
        push_back(curr->data);
    }
}

template <typename T>
T& List<T>::at(const size_t i) {
    Node *node = head.next;
    size_t   n = 0;

    while (n < i && node != NULL) {
        node = node->next;
        n++;
    }

    if (node) {
        return node->data;
    } else {
        throw std::out_of_range("invalid list index");
    }
}

template <typename T>
void List<T>::insert(iterator it, const T& data) {
    if (it == NULL) {
        throw std::out_of_range("invalid iterator");
    }
    it->next = new Node{it->next, data};

    if (it->next->next == NULL) {   // Update tail
        tail = it->next;
    }

    length++;
}

template <typename T>
void List<T>::push_back(const T& data) {
    // Remove list scan
    tail->next = new Node{NULL, data};
    tail = tail->next;
    length++;
}

template <typename T>
void List<T>::erase(iterator it) {
    if (it == NULL) {
        throw std::out_of_range("invalid iterator");
    }

    Node *node = &head;
    while (node != NULL && node->next != it) {
        node = node->next;
    }

    if (node == NULL) {
        throw std::out_of_range("invalid iterator");
    }

    if (it == tail) {               // Update tail
        tail = node;
    }

    node->next = it->next;
    delete it;
    length--;
}

// Main execution -------------------------------------------------------------

int main(int argc, char *argv[]) {
    List<int> list;

    std::cout << "List Size: " << list.size() << std::endl;

    for (int i = 0; i < NITEMS; i++) {
        list.push_back(i);
    }

    std::cout << "List Size: " << list.size() << std::endl;
    std::cout << "List Items:" << std::endl;
    for (size_t i = 0; i < list.size(); i++) {
        std::cout << "List at " << i << " " << list.at(i) << std::endl;
    }

    auto head = list.front();
    list.insert(head, NITEMS + 1);
    list.insert(head, NITEMS + 2);
    list.insert(head->next->next, NITEMS + 3);

    std::cout << "List Size: " << list.size() << std::endl;
    std::cout << "List Items:" << std::endl;
    for (size_t i = 0; i < list.size(); i++) {
        std::cout << "List at " << i << " " << list.at(i) << std::endl;
    }

    List<int> copy(list);

    std::cout << "Copy Size: " << copy.size() << std::endl;
    std::cout << "Copy Items:" << std::endl;
    for (size_t i = 0; i < copy.size(); i++) {
        std::cout << "Copy at " << i << " " << copy.at(i) << std::endl;
    }

    copy.push_back(NITEMS + 4);
    list = copy;
    std::cout << "List Size: " << list.size() << std::endl;
    std::cout << "List Items:" << std::endl;
    for (size_t i = 0; i < list.size(); i++) {
        std::cout << "List at " << i << " " << list.at(i) << std::endl;
    }

    list.erase(list.front());
    list.erase(list.front()->next);
    std::cout << "List Size: " << list.size() << std::endl;
    std::cout << "List Items:" << std::endl;
    for (size_t i = 0; i < list.size(); i++) {
        std::cout << "List at " << i << " " << list.at(i) << std::endl;
    }

    return 0;
}
