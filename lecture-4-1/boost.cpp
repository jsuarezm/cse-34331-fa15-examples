#include <iostream>
#include <functional>
#include <unordered_set>

#include <boost/functional/hash.hpp>

using namespace std;

class Point {
public:
    Point(const double &x, const double &y): x(x), y(y) {}

    bool operator==(const Point &p) const {
    	return x == p.x && y == p.y;
    }

private:
    friend ostream & operator<<(ostream &os, const Point& p);
    friend size_t hash_value(Point const &p);

    double x, y;
};

size_t hash_value(Point const &p) {
    size_t seed = 0;
    boost::hash_combine(seed, p.x);
    boost::hash_combine(seed, p.y);
    return seed;
}

ostream & operator<<(ostream &os, const Point& p) {
    return os << p.x << ", " << p.y;
}

int main(int argc, char *argv[]) {
    unordered_set<Point, boost::hash<Point>> s;

    if (argc != 2) {
    	cerr << "usage: " << argv[0] << " nitems" << endl;
    	return 1;
    }

    int nitems = atoi(argv[1]);

    for (int i = 0; i < nitems; i++) {
    	s.insert(Point(i, i));
    }

    for (int i = 0; i < nitems; i++) {
    	s.find(Point(i, i));
    }

    for (auto p : s) {
    	cout << p << endl;
    }

    return 0;
}
