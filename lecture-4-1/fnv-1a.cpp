#include <cstdint>
#include <iostream>
#include <vector>
#include <set>

using namespace std;

#define TABLE_SIZE  (1<<10)

// http://isthe.com/chongo/tech/comp/fnv/

#define FNV_OFFSET_BASIS    (0xcbf29ce484222325ULL)
#define FNV_PRIME	    (0x100000001b3ULL)

template <typename T>
size_t hash_value(const T &value) {
    uint64_t hash = FNV_OFFSET_BASIS;

    for (uint8_t i = 0; i < sizeof(T); i++) {
    	uint8_t byte = static_cast<uint8_t>(value >> i*8);

    	hash = hash ^ byte;
    	hash = hash * FNV_PRIME;
    }

    return hash;
}

template <typename T>
class SCTable {
public:
    SCTable(int size=TABLE_SIZE) {
    	tsize = size;
	table = vector<set<T>>(tsize);
    }

    void insert(const T &value) {
    	int bucket = hash_value(value) % tsize;

    	table[bucket].insert(value);
    }

    bool find(const T &value) {
    	int bucket = hash_value(value) % tsize;

    	return table[bucket].count(value);
    }

private:
    vector<set<T>> table;
    int tsize;
};

int main(int argc, char *argv[]) {
    SCTable<int> s;

    if (argc != 2) {
    	cerr << "usage: " << argv[0] << " nitems" << endl;
    	return 1;
    }

    int nitems = atoi(argv[1]);

    for (int i = 0; i < nitems; i++) {
    	s.insert(i);
    }

    for (int i = 0; i < nitems; i++) {
    	s.find(i);
    }

    return 0;
}
