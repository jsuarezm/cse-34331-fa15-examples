#include <iostream>
#include <string>

using namespace std;

int main(int argc, char *argv[]) {
    string *s1 = new string("Hi");

    cout << *s1 << ": " << s1->size() << endl;
   
    string *s2 = new string("Yo");
    cout << *s2 << ": " << s2->size() << endl;

    string *s3(s2);
    string *s4 = s2;
    
    cout << *s2 << ": " << s2->size() << endl;
    cout << *s3 << ": " << s3->size() << endl;
    cout << *s4 << ": " << s4->size() << endl;

    return 0;
}
